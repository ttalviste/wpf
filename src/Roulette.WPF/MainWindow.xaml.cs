﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Core;
using Core.Extensions;
using Core.Infrastructure;
using Core.ViewModels;

namespace Roulette.UI.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public HomeViewModel HomeViewModel { get; set; }
        public MainWindow()
        {
            HomeViewModel = new HomeViewModel();
            InitializeComponent();
            DataContext = HomeViewModel;
            Task.Run(() => AsynchronousSocketListener.StartListening(Dns.GetHostName(), 4948, HomeViewModel));
            //Task.Factory.StartNew(() =>
            //    {
            //        AsynchronousSocketListener.StartListening(Dns.GetHostName(), 4948, HomeViewModel);
            //    },TaskCreationOptions.AttachedToParent);

            Unloaded +=OnUnloaded;
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            HomeViewModel.Notifier.Dispose();
        }
    }
}
