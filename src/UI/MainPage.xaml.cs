﻿using System.Collections.Generic;
using System.Linq;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Roulette.UI
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            CreateTable();
            RootGrid.UpdateLayout();
        }

        private void CreateTable()
        {
            int mainGridColumns = 5;
            int mainGridRows = 2;

            for (int i = 0; i < mainGridColumns; i++)
            {
                var item = new ColumnDefinition();
                if (i == 0 || i == 4)
                {
                    item.Width = new GridLength(50, GridUnitType.Star);
                }
                else
                {
                    item.Width = new GridLength(156, GridUnitType.Star);
                }
                RootGrid.ColumnDefinitions.Add(item);
            }
            var rootGridFirstRow = new RowDefinition {Height = new GridLength(150, GridUnitType.Star)};
            var rootGridSecondRow = new RowDefinition {Height = new GridLength(100, GridUnitType.Star)};
            RootGrid.RowDefinitions.Add(rootGridFirstRow);
            RootGrid.RowDefinitions.Add(rootGridSecondRow);

            for (int i = 0; i < mainGridColumns; i++)
            {
                switch (i)
                {
                    case 0:
                        const string zeroBorderKey = "border_0";
                        var border = new Border
                        {
                            AccessKey = zeroBorderKey,
                            Margin = new Thickness(10),
                            Background = new SolidColorBrush(Color.FromArgb(255, 0, 128, 0))
                        };
                        const string zeroElementKey = "number_0";
                        var textBlock = new TextBlock
                        {
                            AccessKey = zeroElementKey,
                            Text = "0",
                            Margin = new Thickness(24),
                            HorizontalAlignment = HorizontalAlignment.Center,
                            VerticalAlignment = VerticalAlignment.Center,
                            FontSize = 22,
                            TextAlignment = TextAlignment.Center,
                            Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255))
                        };
                        border.Child = textBlock;
                        Grid.SetColumn(border, i);

                        RootGrid.Children.Add(border);
                        break;
                    case 1:
                    case 2:
                    case 3:
                        var numberGrid = new Grid();
                        var red = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                        var black = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
                        for (int k = 0; k < 4; k++)
                        {
                            var item = new ColumnDefinition()
                            {
                                Width = new GridLength(156, GridUnitType.Star)
                            };
                            numberGrid.ColumnDefinitions.Add(item);
                        }
                        for (int k = 0; k < 3; k++)
                        {
                            var row = new RowDefinition
                            {
                                Height = new GridLength(120, GridUnitType.Star)
                            };
                            numberGrid.RowDefinitions.Add(row);
                        }
                        if (i == 1)
                        {
                            var numbersSets = Enumerable.Range(1, 12).Batch(3).ToList();
                            AddNumbers(numbersSets, red, black, numberGrid);
                        }
                        else if (i == 2)
                        {
                            var numbersSets = Enumerable.Range(13, 12).Batch(3).ToList();
                            AddNumbers(numbersSets, red, black, numberGrid);
                        }
                        else if (i == 3)
                        {
                            var numbersSets = Enumerable.Range(25, 12).Batch(3).ToList();
                            AddNumbers(numbersSets, red, black, numberGrid);
                        }
                        Grid.SetRow(numberGrid, 0);
                        Grid.SetColumn(numberGrid, i);
                        RootGrid.Children.Add(numberGrid);
                        break;
                    case 4:
                        var grid = new Grid();

                        for (int k = 0; k < 3; k++)
                        {
                            var row = new RowDefinition
                            {
                                Height = new GridLength(120, GridUnitType.Star)
                            };
                            grid.RowDefinitions.Add(row);
                            string borderKey = $"border_2t1_{k}";
                            border = new Border
                            {
                                AccessKey = borderKey,
                                Margin = new Thickness(10),
                                Background = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0))
                            };
                            string elementKey = $"2t1_{k}";
                            textBlock = new TextBlock
                            {
                                AccessKey = elementKey,
                                Text = "2 to 1",
                                Margin = new Thickness(1),
                                HorizontalAlignment = HorizontalAlignment.Center,
                                VerticalAlignment = VerticalAlignment.Center,
                                FontSize = 22,
                                TextAlignment = TextAlignment.Center,
                                Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255))
                            };
                            border.Child = textBlock;
                            Grid.SetRow(border, k);
                            grid.Children.Add(border);
                        }
                        Grid.SetRow(grid, 0);
                        Grid.SetColumn(grid, i);
                        RootGrid.Children.Add(grid);
                        break;
                }
            }
        }

        private static void AddNumbers(List<List<int>> numbersSets, SolidColorBrush red, SolidColorBrush black, Grid numberGrid)
        {
            int column = 0;
            bool isRed = true;
            foreach (var set in numbersSets)
            {
                int row = 2;
                if (column == 1)
                {
                    isRed = false;
                }

                foreach (int item in set)
                {
                    string borderKey = $"border_{item}";
                    var tmpBorder = new Border
                    {
                        AccessKey = borderKey,
                        Margin = new Thickness(10),
                        Background = isRed ? red : black
                    };
                    
                    string numberKey = $"number_{item}";
                    var tmpTextBlock = new TextBlock
                    {
                        AccessKey = numberKey,
                        Text = item.ToString(),
                        Margin = new Thickness(1),
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center,
                        FontSize = 22,
                        TextAlignment = TextAlignment.Center,
                        Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255))
                    };
                    tmpBorder.Child = tmpTextBlock;

                    Grid.SetColumn(tmpBorder, column);
                    Grid.SetRow(tmpBorder, row);
                    numberGrid.Children.Add(tmpBorder);

                    isRed = !isRed;
                    row--;
                    
                }
                column++;
            }
            
        }
    }
}
