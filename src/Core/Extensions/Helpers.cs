﻿using System.Collections.Generic;
using Core.ViewModels.CustomMessages;
using ToastNotifications;

namespace Core.Extensions
{
    public static class Helpers
    {
        public static IEnumerable<List<T>> Batch<T>(this IEnumerable<T> collection, int batchSize)
        {
            List<T> nextbatch = new List<T>(batchSize);
            foreach (T item in collection)
            {
                nextbatch.Add(item);
                if (nextbatch.Count == batchSize)
                {
                    yield return nextbatch;
                    nextbatch = new List<T>(batchSize);
                }
            }
            if (nextbatch.Count > 0)
                yield return nextbatch;
        }

        public static IEnumerable<int> GetWithStep(int start, int count, int step)
        {
            var list = new List<int>();
            list.Add(start);
            int temp = start + step;
            for (int i = 1; i < count; i++)
            {
                list.Add(temp);
                temp = temp + step;
            }
            return list;
        }

        public static void ShowWinningMessage(this Notifier notifier, string title, List<string> message)
        {
            notifier.Notify<WinningMessageNotification>(() => new WinningMessageNotification(title, message));
        }
    }
}
