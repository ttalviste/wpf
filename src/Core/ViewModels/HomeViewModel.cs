﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Core.Data;
using Core.Extensions;
using ReactiveUI;
using ToastNotifications;
using ToastNotifications.Core;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages.Core;
using ToastNotifications.Messages.Information;
using ToastNotifications.Position;

namespace Core.ViewModels
{
    public class HomeViewModel : ReactiveObject
    {
        public Notifier Notifier;
        public List<TableField> Fields { get; set; }

        string _result;

        public string Result
        {
            get => _result;
            set => this.RaiseAndSetIfChanged(ref _result, value);
        }

        public ReactiveCommand<string,List<string>> UpdateTable { get; protected set; }

        public ReactiveCommand<List<string>,bool> DisplayMessage { get; protected set ;}

        readonly ObservableAsPropertyHelper<List<string>> _tableResults;

        public List<string> TableResults => _tableResults.Value;

        public HomeViewModel()
        {
            Notifier = new Notifier(cfg =>
            {
                cfg.Dispatcher = Application.Current.Dispatcher;
                cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(TimeSpan.FromSeconds(10), MaximumNotificationCount.FromCount(2));
                cfg.PositionProvider = new WindowPositionProvider(Application.Current.MainWindow,Corner.BottomRight, 65,70);
            });
            UpdateTable = ReactiveCommand.CreateFromTask<string, List<string>>(GetUpdatedFields);

            this.WhenAnyValue(vm => vm.Result)
                .Select(x => x?.Trim())
                .DistinctUntilChanged()
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .InvokeCommand(UpdateTable);

            UpdateTable.ThrownExceptions.Subscribe(ex => {/* Handle errors here */});

            _tableResults = UpdateTable.ToProperty(this, vm => vm.TableResults, new List<string>());
            
            Fields = AddFields();



        }


        public async Task<List<string>> GetUpdatedFields(string ball)
        {
            var list = new List<string>();
            int winningValue = int.Parse(ball);
            var winningFields = Fields.Where(x => x.Value == winningValue || (x.AssociatedValues != null && x.AssociatedValues.Contains(winningValue))).ToList();
            winningFields.ForEach(i =>
            {
                i.IsHighLighted = true;
            });
            var notWinning = Fields.Where(x => !winningFields.Select(y => y.Title).Contains(x.Title) && x.IsHighLighted)
                .ToList();
            notWinning.ForEach(
                i =>
                {
                    i.IsHighLighted = false;
                });
            list.AddRange(winningFields.Where(x => x.Value != winningValue).Select(x => x.Title));
            Notifier.ShowWinningMessage(ball,list);
            
            return await Task.FromResult(list);
        }

        private List<TableField> AddFields()
        {
            var result = new List<TableField>(49);

            var zero = new TableField
            {
                Title = "0",
                Text = "0",
                Value = 0,
                IsRed = false,
                DefaultColor = Constants.ApplicationColors.Green.ToString()
            };
            zero.IsHighLighted = false;
            result.Add(zero);

            var firstBlock = Enumerable.Range(1, 12).Batch(3).ToList();
            AddNumbers(firstBlock, result);

            var secondBlock = Enumerable.Range(13, 12).Batch(3).ToList();
            AddNumbers(secondBlock, result);

            var thirdBlock = Enumerable.Range(25, 12).Batch(3).ToList();
            AddNumbers(thirdBlock, result);

            var twoToOne0 = new TableField
            {
                Title = "TwoToOne0",
                Text = "2 to 1",
                IsRed = false,
                DefaultColor = Constants.ApplicationColors.Black.ToString()

            };
            twoToOne0.AssociatedValues = Helpers.GetWithStep(1, 12, 3).ToList();
            twoToOne0.IsHighLighted = false;
            result.Add(twoToOne0);

            var twoToOne1 = new TableField
            {
                Title = "TwoToOne1",
                Text = "2 to 1",
                IsRed = false,
                DefaultColor = Constants.ApplicationColors.Black.ToString()

            };
            twoToOne1.AssociatedValues = Helpers.GetWithStep(2,12,3).ToList();
            twoToOne1.IsHighLighted = false;
            result.Add(twoToOne1);

            var twoToOne2 = new TableField
            {
                Title = "TwoToOne2",
                Text = "2 to 1",
                IsRed = false,
                DefaultColor = Constants.ApplicationColors.Black.ToString()

            };
            twoToOne2.AssociatedValues = Enumerable.Range(3, 34).Where(x => x % 3 == 0).ToList();
            twoToOne2.IsHighLighted = false;
            result.Add(twoToOne2);

            var firstTweleve = new TableField
            {
                Title = "First12",
                Text = "1st 12",
                IsRed = false,
                DefaultColor = Constants.ApplicationColors.Black.ToString()

            };
            firstTweleve.AssociatedValues = Enumerable.Range(1, 12).ToList();
            firstTweleve.IsHighLighted = false;
            result.Add(firstTweleve);

            var oneTo18 = new TableField
            {
                Title = "OneTo18",
                Text = "1 to 18",
                IsRed = false,
                DefaultColor = Constants.ApplicationColors.Black.ToString()

            };
            oneTo18.AssociatedValues = Enumerable.Range(1, 17).ToList();
            oneTo18.IsHighLighted = false;
            result.Add(oneTo18);

            var even = new TableField
            {
                Title = "Even",
                Text = "Even",
                IsRed = false,
                IsOdd = false,
                DefaultColor = Constants.ApplicationColors.Black.ToString()

            };
            even.AssociatedValues = result.Where(x => x.Value.HasValue && x.Value != 0 && !x.IsOdd).Select(x => x.Value.Value).ToList();
            even.IsHighLighted = false;
            result.Add(even);

            var second12 = new TableField
            {
                Title = "Second12",
                Text = "2nd 12",
                IsRed = false,
                DefaultColor = Constants.ApplicationColors.Black.ToString()

            };
            second12.AssociatedValues = Enumerable.Range(13, 12).ToList();
            second12.IsHighLighted = false;
            result.Add(second12);

            var black = new TableField
            {
                Title = "Black",
                Text = "",
                IsRed = false,
                DefaultColor = Constants.ApplicationColors.Black.ToString()

            };
            black.AssociatedValues = result.Where(x => x.Value.HasValue && !x.IsRed && x.Value != 0).Select(x => x.Value.Value).ToList();
            black.IsHighLighted = false;
            result.Add(black);

            var red = new TableField
            {
                Title = "Red",
                Text = "",
                IsRed = true,
                DefaultColor = Constants.ApplicationColors.Red.ToString()

            };
            red.AssociatedValues = result.Where(x => x.Value.HasValue && x.IsRed).Select(x => x.Value.Value).ToList();
            red.IsHighLighted = false;
            result.Add(red);

            var third12 = new TableField
            {
                Title = "Third12",
                Text = "3rd 12",
                IsRed = false,
                DefaultColor = Constants.ApplicationColors.Black.ToString()

            };
            third12.AssociatedValues = Enumerable.Range(25, 12).ToList();
            third12.IsHighLighted = false;
            result.Add(third12);

            var odd = new TableField
            {
                Title = "Odd",
                Text = "Odd",
                IsRed = false,
                IsOdd = true,
                DefaultColor = Constants.ApplicationColors.Black.ToString()
            };
            odd.AssociatedValues = result.Where(x => x.Value.HasValue && x.IsOdd).Select(x => x.Value.Value).ToList();
            odd.IsHighLighted = false;
            result.Add(odd);

            var nineTeenTo36 = new TableField
            {
                Title = "NineTeenTo36",
                Text = "19 to 36",
                IsRed = false,
                DefaultColor = Constants.ApplicationColors.Black.ToString()
            };
            nineTeenTo36.AssociatedValues = Enumerable.Range(19, 17).ToList();
            nineTeenTo36.IsHighLighted = false;
            result.Add(nineTeenTo36);

            return result;
        }

        private void AddNumbers(List<List<int>> numbersSets, List<TableField> result)
        {
            int column = 0;
            bool isRed = true;
            foreach (var set in numbersSets)
            {
                if (column == 1)
                {
                    isRed = false;
                }

                foreach (int item in set)
                {
                    var newTableField = new TableField();
                    newTableField.Title = item.ToString();
                    newTableField.Text = item.ToString();
                    newTableField.Value = item;
                    newTableField.IsOdd = item % 2 != 0;
                    newTableField.IsHighLighted = false;
                    newTableField.IsRed = isRed;
                    newTableField.DefaultColor = isRed
                        ? Constants.ApplicationColors.Red.ToString()
                        : Constants.ApplicationColors.Black.ToString();
                    
                    result.Add(newTableField);
                    isRed = !isRed;
                }
                column++;
            }
        }
    }
}
