﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToastNotifications.Core;

namespace Core.ViewModels.CustomMessages
{
    /// <summary>
    /// Interaction logic for WinningMessageNotificationDisplayPart.xaml
    /// </summary>
    public partial class WinningMessageNotificationDisplayPart : NotificationDisplayPart
    {
        private WinningMessageNotification winningMessageNotification;


        public WinningMessageNotificationDisplayPart(WinningMessageNotification winningMessageNotification)
        {
            this.winningMessageNotification = winningMessageNotification;
            DataContext = winningMessageNotification;
            InitializeComponent();
        }
    }
}
