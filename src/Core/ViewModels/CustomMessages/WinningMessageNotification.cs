using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using ToastNotifications.Core;

namespace Core.ViewModels.CustomMessages
{
    public class WinningMessageNotification : NotificationBase, INotifyPropertyChanged
    {
        private WinningMessageNotificationDisplayPart _displayPart;

        public override NotificationDisplayPart DisplayPart => _displayPart ?? (_displayPart = new WinningMessageNotificationDisplayPart(this));

        public WinningMessageNotification(string title, List<string> message)
        {
            Title = title;
            Message = message;
        }

        #region binding properties
        private string _title;
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                OnPropertyChanged();
            }
        }

        private List<string> _message;
        public List<string> Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}