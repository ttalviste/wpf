﻿namespace Core.Data
{
    public class IncomingResult
    {
        public IncomingResult()
        {
            Data = new PayLoad();
        }
        public string Qualifier { get; set; }
        public long Correlation { get; set; }
        public PayLoad Data { get; set; }
    }

    public class PayLoad
    {
        public int WinningNumber { get; set; }
    }
}
