﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Media;
using ReactiveUI;

namespace Core.Data
{
    public class TableField : ReactiveObject
    {
        public string DefaultColor { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public bool IsOdd { get; set; }

        public bool IsRed { get; set; }

        public int? Value { get; set; }

        public List<int> AssociatedValues { get; set; }

        bool _isHighLighted;

        public bool IsHighLighted
        {
            get => _isHighLighted;
            set => this.RaiseAndSetIfChanged(ref _isHighLighted, value);
        }

        public ReactiveCommand<bool, string> HighLight { get; protected set; }

        readonly ObservableAsPropertyHelper<string> _stateColor;

        public string StateColor => _stateColor.Value;
        

        public TableField()
        {
            IsHighLighted = true;
            HighLight = ReactiveCommand.CreateFromTask<bool, string>(HighLightField);

            this.WhenAnyValue(x => x.IsHighLighted)
                .DistinctUntilChanged()
                .InvokeCommand(HighLight);


            HighLight.ThrownExceptions.Subscribe(ex => {/* Handle errors here */});

            _stateColor = HighLight.ToProperty(this, x => x.StateColor, out _stateColor,Colors.Black.ToString());
        }

        public async Task<string> HighLightField(bool highLigth)
        {
            if (highLigth)
            {
                return await Task.FromResult(Constants.ApplicationColors.Blue.ToString());
            }
            if (Text == "0")
            {
                return await Task.FromResult(Constants.ApplicationColors.Green.ToString());
            }
            if (IsRed)
            {
                return await Task.FromResult(Constants.ApplicationColors.Red.ToString());
            }
            if (Value.HasValue && IsOdd)
            {
                IsRed = true;
                return await Task.FromResult(Constants.ApplicationColors.Red.ToString());
            }
            IsRed = false;
            return await Task.FromResult(Constants.ApplicationColors.Black.ToString());
        }
    }
}
