﻿using System.Windows.Media;

namespace Core
{
    public static class Constants
    {
        public static class ApplicationColors
        {
            public static Color Black = Color.FromArgb(255, 0, 0, 0);
            public static Color Red = Color.FromArgb(255, 255, 0, 0);
            public static Color White = Color.FromArgb(255, 255, 255, 255);
            public static Color Green = Color.FromArgb(255, 0, 128, 0);
            public static Color Blue = Colors.Blue;
        }

        public static class Brushes
        {
            public static SolidColorBrush BlackColorBrush = new SolidColorBrush(ApplicationColors.Black);
            public static SolidColorBrush RedColorBrush = new SolidColorBrush(ApplicationColors.Red);
            public static SolidColorBrush WhiteColorBrush = new SolidColorBrush(ApplicationColors.White);
            public static SolidColorBrush GreenColorBrush = new SolidColorBrush(ApplicationColors.Green);
            public static SolidColorBrush BlueColorBrush = new SolidColorBrush(ApplicationColors.Blue);
        }
    }
}
